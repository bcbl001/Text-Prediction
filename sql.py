#######################################
### Ben Langley
### SQL methods for prediction.py to load
###     and store data as a list
###
### To use in another file
###     import sql
###
### To debug run
###     python sql.py
#######################################
#!/usr/bin/python

import sqlite3
from sqlite3 import Error

class Database:

    ### Create a database conntection to the SQLite database
    ###     specified by the bd_file
    ### @param the database file
    def __init__(self, db_file):
        try:
            self.conn = sqlite3.connect(db_file)
        except Error as e:
            print(e)

    ### Query all rows in the table
    ### @param table the table to get data from
    ### @return all rows of the table as a list
    def select_all(self, table):
        cur = self.conn.cursor()
        query = "SELECT * FROM " + table
        cur.execute(query)
        rows = cur.fetchall()
        return rows

    ### Get the messages which sent by me
    ### @return a list of all the messages
    def my_messages(self):
        messageTable = "message"
        rows = self.select_all(messageTable)
        myMessages = []
        messageNum = len(rows) - 1
        while messageNum > 0:
            message = rows[messageNum]
            if message[16] != message[17] and message[2] is not None:
                myMessages.append(message[2])
            messageNum -= 1
        return myMessages

####### FOR DEBUG ########
'''
db_file= "messages.db"
database = Database(db_file)
my_messages = database.my_messages()
print my_messages[0]
'''
