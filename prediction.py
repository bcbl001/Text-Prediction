#################################
### Ben Langley
### Text prediction for imessages
###
### To run use
###   python prediction.py
################################
from sql import Database
import sys

### Remove punctuation from the message
### @param messages a list of all the messages
### @return a list of all the cleaned messages
def removePunctuation(messages):
    removeChars = ['!','.',',','?'] # ! . , ?
    cleanMessages = []
    for message in messages:
        for punctuationChar in removeChars:
            message = message.replace(punctuationChar, "")
        cleanMessages.append(message)
    return cleanMessages

### Create the dictionary of dictionaries used for the word prediction
### @param messages a list of all the messages
### @return a dictionary of words with next word freqencies
def generatePredictions(messages):
    ## Dictioanary of dictionary
    ##  Key is the word
    ##  Value is a dictionary of following words
    ##      Key is the word
    ##      Value is the freq of the word
    wordPredictions = {}

    ## Go through each message and add to the dictionary of words
    for message in messages:
        if type(message) == type(None):
            print "None type message: ",message
            continue

        ## Split the message into each individual word
        words = message.split(" ")

        ## Go through each word which has a next word and add to the dictionary of dictionaries
        index = 0
        while index < (len(words) - 1):
            # Get the current word and next word
            word = words[index]
            nextWord = words[index + 1]

            # Convert to lower case
            word = word.lower()
            nextWord = nextWord.lower()

            # Checj if the current word is in the wordPredictions dictionary
            if word in wordPredictions:
                nextWordFreq = wordPredictions[word]
            else:
                nextWordFreq = {}

            # Check if the nextWord is already in the current words frequency table
            if nextWord in nextWordFreq:
                # nextWord already has a frequency so increment it
                nextWordFreq[nextWord] += 1
            else:
                # nextWord is a new word for the frequency table
                nextWordFreq[nextWord] = 1

            # Update the wordPrediction dictionary
            wordPredictions[word] = nextWordFreq

            index += 1

    return wordPredictions

### Converts the frequency table to a table of probabilities (relative frequency)
### @param wordPredictionsDictionary the frequency table dictionary
### @return the dictionary with relative frequencies
def convertToPercents(wordPredictionsDictionary):
    ## Go through each word in the dictionary
    for word in wordPredictionsDictionary:
        nextWordDictionary = wordPredictionsDictionary[word]
        # Calculate total number of next words
        total = 0
        for nextWord in nextWordDictionary:
            total += nextWordDictionary[nextWord]

        # Change each frequency to a percent
        for nextWord in nextWordDictionary:
            nextWordDictionary[nextWord] = round(float(nextWordDictionary[nextWord]) / float(total), 3)

        # Update wordPredictionsDictionary
        wordPredictionsDictionary[word] = nextWordDictionary

    return wordPredictionsDictionary

### Get the nextWord sorted for word
### @param wordPredictions the dictionary of frequencies
### @param word the word to find the next words for
### @return a dictionary of the most probable next word (highest probability first)
def getNextWordProb(wordPredictions, word):
    # Convert word to lowercase
    word = word.lower()

    #Check word is in wordPredictions
    if word not in wordPredictions:
        return []

    # The word is in the dictionary, get nextWord dictionary
    nextWordDictionary = wordPredictions[word]

    # Sort the dictionary
    sortedNextWord = sorted(nextWordDictionary.iteritems(), key=lambda (k,v): (v,k), reverse=True)

    return sortedNextWord

### Learns for specified amount of data
### @param messagesToLoad the number of text messages to learn from
### @return word relative frequency table
def loadData(messagesToLoad):
    # Database variables
    db_file = "messages.db"
    database = Database(db_file)

    if database is None:
        print "Error connecting to db"
        return None

    myMessages = database.my_messages()

    # FOR DEBUG
    if messagesToLoad == 0:
        messagesToLoad = len(myMessages) - 1

    print "Loading",messagesToLoad,"messages"
    myMessages = myMessages[0:messagesToLoad]

    # Clean the messages
    myMessages = removePunctuation(myMessages)

    wordPredictions = generatePredictions(myMessages)
    percents = convertToPercents(wordPredictions)
    return percents

#mostProbableYou = getNextWordProb(percents, "you")
#print mostProbableYou
