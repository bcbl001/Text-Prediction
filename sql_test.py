#!/usr/bin/python

import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def select_all_tasks(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM tasks")

    rows = cur.fetchall()

    for row in rows:
        print(row)

def main():
    database = "messages.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        print("Table names:")
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        print(cursor.fetchall())
        print("First 5 messages:")
        cursor.execute("SELECT * FROM message")
        rows = cursor.fetchall()
        latestMessage = len(rows)
        print "NUmber of messages:",latestMessage
        i = 1
        myMessages = 0
        while myMessages < 10:
            message = rows[latestMessage - i]
            if message[16] != message[17]:
                print message[2]
                myMessages += 1
            i += 1
        '''
        print rows[latestMessage - 1][2]
        print rows[latestMessage - 2][2]
        print rows[latestMessage - 3][2]
        print rows[latestMessage - 4][2]
        print rows[latestMessage - 5][2]
        '''
        #print type(rows[0])
        print("Query all tasks")
        #select_all_tasks(conn)


if __name__ == '__main__':
    main()
